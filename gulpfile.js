var gulp = require('gulp');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');

gulp.task('default', function() {
  // place code for your default task here
  gulp.run('watch');
});

gulp.task('scripts', function() {
    // corpo da tarefa 
    return gulp
            .src(['public/js/**/*.js'])
            .pipe(uglify())
            .pipe(gulp.dest('public/build/js'));      
});

gulp.task('watch', function(){
  
  gulp.watch('public/js/**/*.js', function(event){
    gutil.log('File '+event.path+' was '+event.type+', running tasks...');
    gulp.run('scripts');
  });
});