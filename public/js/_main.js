angular.module('app', ['app.routes', 'ngResource']).config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'http://agenciamilk.api3.nextbigsound.com**',
    'https://agenciamilk.api3.nextbigsound.com**'
  ]);
 });
