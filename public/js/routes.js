angular.module('app.routes', ['ngRoute']) // startando a propriedade module do objeto angular e iniciando a app
.config(function($routeProvider) {

  $routeProvider.when('/', {
    templateUrl: 'js/login/login.html',
    controller: 'LoginCtrl'
  });

  $routeProvider.when('/admin', {
    templateUrl: 'js/admin/admin.html',
    controller: 'AdminCtrl'
  });

  $routeProvider.when('/analytics', {
    templateUrl: 'js/analytics/analytics.html',
    controller: 'AnalyticsCtrl'
  });

  $routeProvider.when('/analytics/'+FORM_URL, {
    templateUrl: 'forms/'+FORM_URL+'/index.html',
    controller: 'AnalyticsCtrl'
  });

  $routeProvider.otherwise({redirectTo: '/'});

});
