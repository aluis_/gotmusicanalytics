angular
  .module('app')
  .controller('AnalyticsCtrl', ['$scope', '$http', '$location', AnalyticsCtrl]);

function AnalyticsCtrl($scope, $http, $location) {

    $scope.artistName = '--';
    $scope.artistImg = 'img/logoMilk.jpg';
    $scope.realtime = '--';
    $scope.metricsArtist = "";
    $scope.loading = false;
    $scope.metricsArtist = 0;
    $scope.allDownloads = '--';
    $scope.allShowRequests = '--';
    $scope.formUrl = FORM_URL;

//TODO: MAKE A SERVICE
   var currUser = (function() {
       var user = "";

       (function init(){
           if(sessionStorage.currUser && sessionStorage.currUser !== undefined) {
               user = JSON.parse(sessionStorage.currUser);
               console.log('user', user);
               NBS_ARTIST_ID = user.info.NBS_ARTIST_ID;
               MESSAGE_FORM_ID = user.info.MESSAGE_FORM_ID;
               $scope.artistName = user.info.name;
               if(user.info.img.length > 1) {
                   $scope.artistImg = user.info.img;
               }
           } else {
               console.log('sem sessao');
               $location.url("/");
           }
       })();

       return ({
           getUser: function(){
               return user;
           },
           getName: function(){
               return user.info.name;
           },
           getToken: function(){
               return user.token;
           },
           destroy: function(){
               sessionStorage.currUser = "";
               $location.url("/");
           }
       });
   })();


   $scope.logout = function(){
       currUser.destroy();
   };



  //verify token
    $http.post(
      "/api/verifyToken", {'token': currUser.getToken})
    .success(function(response) {
        if (!response.success) {
            $location.url("/");
        }
    });


// TODO: MAKE A SERVICE
  $http.get(
    "//agenciamilk.api3.nextbigsound.com/metrics/artist/" + NBS_ARTIST_ID + ".json")
  .success(function(response) {
    loading = true;
    $scope.metricsArtist = response;

  }).finally(function(){
     loading = false;
  });

  $scope.getLast = function(arg){
    var arr = [];
    for (var prop in arg) {
      arr.push(arg[prop]);
    }
    if (arr.pop() === undefined){
      return "nenhum";
    }else{
        if(arr.pop() >= 1000 && arr.pop() < 1000000){
            result = arr.pop().toString().slice(0, -3);
            return result+"K";
        }else if (arr.pop() >= 1000000 ) {
            result = arr.pop().toString().slice(0, -6);
            return result+"KK";
        }else{
            return arr.pop();
        }

    }
  };

  var convertNumbers = function(arg){
        if(arg >= 1000 && arg < 1000000){
            result = arg.toString().slice(0, -3);
            return result+"K";
        }else if (arg >= 1000000 ) {
            result = arg.toString().slice(0, -6);
            return result+"KK";
        }else{
            return arg;
        }
  };

/* piwik analytics TODO: MAKE A SERVICE
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
*/
//ANALYTICS_SITE_ID
$http.get('/api/analytics/realtime/'+ ANALYTICS_SITE_ID).success(function(resp) {
    $scope.realtime = resp[0].visits;
});

/* APP installs TODO: MAKE A SERVICE
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
*/
$http.get('//googleplay-jsapi.herokuapp.com/app/'+APP_ID).success(function(e) {

    var first = parseInt(e.numDownloads.split(' ')[1].replace(',',''));
    var last = parseInt(e.numDownloads.split(' ')[3].replace(',',''));

    $scope.allDownloads = convertNumbers(first) + ' - ' + convertNumbers(last);
});



/* Peditos de show TODO: MAKE A SERVICE
 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
*/
  $scope.spreadsheetID = MESSAGE_FORM_ID;
  var allMails = [];

  // Message API
  $http.get('//spreadsheets.google.com/feeds/worksheets/' + MESSAGE_FORM_ID + '/public/full?alt=json')
      .success(function(e) {
          var worksheet_url = e.feed.entry[0].link[0].href.split('/');
          MESSAGE_WORKSHEET_ID = worksheet_url[worksheet_url.length - 3];

      $http.get('//spreadsheets.google.com/feeds/list/' + MESSAGE_FORM_ID + '/' + MESSAGE_WORKSHEET_ID + '/public/full?alt=json')
          .success(function(e) {
              var entries = e.feed.entry;

              for (var i = 0; i < entries.length; i++) {
                  var entry = entries[i];
                  var item;
                  try {
                      item = {
                          timestamp: entry.gsx$timestamp.$t,
                          email: entry.gsx$email.$t,
                          cidade: entry.gsx$cidade.$t
                      };
                  } catch (exception) {
                      console.log('Malformed spreadsheet', exception); //  substituido o alert por console.log, para nao aparecer para o usuário final em caso de erros

                  }

                  allMails.push(item);

              }

              $scope.allShowRequests = entries.length;

          }).error(function(e) {
              $log.info('Error: ' + e);
          });
      })
      .error(function(e) {
      });

}
