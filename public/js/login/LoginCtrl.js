angular.module('app').controller('LoginCtrl',['$scope','$http', '$location', function($scope, $http, $location) {

  $scope.auth = function(){

      //verify token
        $http.post(
          "/api/authenticate", {'name': $scope.login, 'password': $scope.password})
        .success(function(response) {
            console.log(response);
            if (response.success) {
                sessionStorage.currUser = JSON.stringify({
                    info: response.user.info,
                    token: response.token
                });
                $location.url("/analytics");
            }
        });
    };
}]);
