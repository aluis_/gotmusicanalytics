angular.module('app', ['app.routes', 'ngResource']);
//aa  ss

//Shows in app
var MESSAGE_FORM_ID = '1S5SP9g3fnNLFYsL3Abzk5eucDlIZFOzTslKqLQ-A5Yc';
var MESSAGE_WORKSHEET_ID;

var NBS_ARTIST_ID = 287163;

var TOKEN = "";

angular
  .module('app')
  .controller('AnalyticsCtrl', ['$scope', '$http', '$location', AnalyticsCtrl]);

function AnalyticsCtrl($scope, $http, $location) {
   var currUser = (function() {
       var user = "";

       if(sessionStorage.currUser && sessionStorage.currUser !== undefined) {
           user = JSON.parse(sessionStorage.currUser);
       } else {
           console.log('sem sessao');
           $location.url("/");
       }

       return ({
           getUser: function(){
               return user;
           },
           getToken: function(){
               return user.token;
           },
           destroy: function(){
               sessionStorage.currUser = "";
               $location.url("/");
           }
       });
   })();

   $scope.logout = function(){
       currUser.destroy();
   };

  $scope.metricsArtist = "";
  $scope.loading = false;

  $scope.metricsArtist = 0;

  //verify token
    $http.post(
      "http://localhost:3000/api/verifyToken", {'token': currUser.getToken})
    .success(function(response) {
        console.log(response);
        if (!response.success) {
            $location.url("/");
        }
    });



  $http.get(
    "http://agenciamilk.api3.nextbigsound.com/metrics/artist/" + NBS_ARTIST_ID + ".json")
  .success(function(response) {
    loading = true;
    console.log('http',response);
    $scope.metricsArtist = response;

  }).finally(function(){
     loading = false;
  });

  console.log('oi', $scope.metricsArtist);

  $scope.getLast = function(arg){
    var arr = [];
    for (var prop in arg) {
      arr.push(arg[prop]);
    }
    if (arr.pop() === undefined){
      return "nenhum";
    }else{
        if(arr.pop() >= 1000 && arr.pop() < 1000000){
            result = arr.pop().toString().slice(0, -3);
            return result+"K";
        }else if (arr.pop() >= 1000000 ) {
            result = arr.pop().toString().slice(0, -6);
            return result+"KK";
        }else{
            return arr.pop();
        }

    }
  };

/* Peditos de show
 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
*/
  $scope.spreadsheetID = MESSAGE_FORM_ID;
  var allMails = [];

  $scope.allShowRequests = 0;

  // Message API
  $http.get('https://spreadsheets.google.com/feeds/worksheets/' + MESSAGE_FORM_ID + '/public/full?alt=json')
      .success(function(e) {
          var worksheet_url = e.feed.entry[0].link[0].href.split('/');
          MESSAGE_WORKSHEET_ID = worksheet_url[worksheet_url.length - 3];

      $http.get('https://spreadsheets.google.com/feeds/list/' + MESSAGE_FORM_ID + '/' + MESSAGE_WORKSHEET_ID + '/public/full?alt=json')
          .success(function(e) {
              var entries = e.feed.entry;

              for (var i = 0; i < entries.length; i++) {
                  var entry = entries[i];
                  var item;
                  try {
                      item = {
                          timestamp: entry.gsx$timestamp.$t,
                          email: entry.gsx$email.$t,
                          cidade: entry.gsx$cidade.$t
                      };
                  } catch (exception) {
                      console.log('Malformed spreadsheet', exception); //  substituido o alert por console.log, para nao aparecer para o usuário final em caso de erros

                  }

                  allMails.push(item);

              }

              $scope.allShowRequests = entries.length;

          }).error(function(e) {
              $log.info('Error: ' + e);
          });
      })
      .error(function(e) {
      });

}

angular.module('app').controller('LoginCtrl',['$scope','$http', '$location', function($scope, $http, $location) {

  $scope.auth = function(){

      //verify token
        $http.post(
          "http://localhost:3000/api/authenticate", {'name': $scope.login, 'password': $scope.password})
        .success(function(response) {
            console.log(response);
            if (response.success) {
                sessionStorage.currUser = JSON.stringify({
                    name: $scope.login,
                    password: $scope.password,
                    token: response.token
                });
                $location.url("/analytics");
            }
        });
    };
}]);

angular.module('app.routes', ['ngRoute']) // startando a propriedade module do objeto angular e iniciando a app
.config(function($routeProvider) {

  $routeProvider.when('/', {
    templateUrl: 'js/login/login.html',
    controller: 'LoginCtrl' 
  });

  $routeProvider.when('/analytics', {
    templateUrl: 'js/analytics/analytics.html',
    controller: 'AnalyticsCtrl'
  });

  $routeProvider.otherwise({redirectTo: '/'});

});

angular.module('app').service('TesteStore', function () {
  var teste  = 1;
  
  var result = 0;

  
  return {
    getTeste: function () {
      return teste;
    },
    setTeste: function(value) {
      teste = value;
    }
    
  };
});
angular
  .module('app')
  .service('nextBigSoundFactory', ['$http', nextBigSoundFactory]);

function nextBigSoundFactory($http){
  var resp;
 
  
  function getNextBigSound(artistId){
  // var loading = true;

      $http.get(
        "http://agenciamilk.api3.nextbigsound.com/metrics/artist/"
        +artistId+".json")
      .success(function(response) {
        
        console.log('http',response)
        resp = response;
        
      //}).finally(function(){
    //     loading = false;
      });
  } 
  
  
   return {
    getMetrics: function(artistId){
      return getNextBigSound(artistId);
      console.log('resp',resp)
       //return resp;
      
    }
  };
}
