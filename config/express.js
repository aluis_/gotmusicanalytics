var express = require('express'); // importa o módulo do express
var app = express(); //carregando o express
var bodyParser = require('body-parser'); // importa o body-parser que permite parsear as requisições
var morgan = require('morgan'); // log do node
var mongoose = require('mongoose'); // orm domongo

var jwt = require('jsonwebtoken'); // usado para criar, assimilar e verificar tokiens

var config = require("./config");
var User = require('../application/models/user');
var auth = require("../application/routes/auth");

//piwik analytics
var PiwikClient = require('piwik-client');
var myClient = new PiwikClient('http://andrearaujoweb.com/piwik/index.php', '30b4abb1d7d7f8dac9f1dee998ffe493' );


/* torna o que estiver dentro da funcção export visível para os arquivos que derem require */
module.exports = function() {

    app.get('/api/analytics/realtime/:id', function(req, res, next) {
        try {
            var resp = {};
            myClient.api({
             method:   'Live.getCounters',
             idSite:   req.params.id,
             period:   'day',
             date:     'today',
             lastMinutes:   1
            },function (err, responseObject){
               resp.err = err;
               resp.ok = responseObject;
               console.log( resp.err,  resp.ok);
               res.json(resp.ok);
               next();
            });
        } catch(e) {
            res.json('error on piwik real-time');
        }
    });

    // =======================
    // configuration =========
    // =======================

    var allowCrossDomain = function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      next();
    };

    app.use(allowCrossDomain);

    var port = process.env.PORT || 3000; // used to create, sign, and verify tokens
    mongoose.connect(config.databaseProduction || config.databaseDev); // connect to database
    app.set('superSecret', config.secret); // secret variable

    app.set('port', port); // seta o valor da variável de ambiente porta para 3000

    /* middleware */
    app.use(express.static('./public')); // utiliza o express.static, que define pastas estáticas

    app.set('view engine', 'ejs'); // utiliza o ejs como template engine ao mudar a variável de ambiente view engine
    app.set('views','./app/views'); // utiliza o a pasta './app/views' como diretório padrão para as views ao mudar variável de ambiente 'views'

    app.use(bodyParser.urlencoded({extended: false})); // permite requisições com o corpo x-ww-form-urlencoded, permitindo acessar os dados da requisição atravésde req.body
    app.use(bodyParser.json()); // permite realizar parse de JSON

    app.use(require('method-override')()); // Possibilita o uso de verbos HTTP como o PUT e o DELETE quando o client não der suporte a eles

    //routes
    auth(app, jwt, User);

    // log do node
    app.use(morgan('dev'));

    return app; // retorna a função express()
};
