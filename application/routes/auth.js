module.exports = function(app, jwt, User) {

    var authCtrl = require("../../application/controllers/auth.ctrl")(app, jwt, User);
    var createUserCtrl = require("../../application/controllers/createUser.ctrl")(app, jwt, User);

    app.get('/api/', authCtrl.index);
    app.post('/api/authenticate', authCtrl.authenticate); //name, password
    app.post('/api/verifyToken', authCtrl.verifyToken); //token
    app.get('/api/showUsers', authCtrl.showUsers);
    app.get('/api/createAdmin', authCtrl.createAdmin);

    //Create Users
    app.get('/api/createUser/abraham', createUserCtrl.createAbraham);


};
