module.exports = function(app, jwt, User) {
    //var Usuario = app.models.usuario;
    var authController = {
        index: function(req, res) {
            res.json({teste:'aa'});
        },
        authenticate: function(req, res) {
            console.log(req.body);
            User.findOne({
                name: req.body.name
                }, function(err, user) {

                if (err) throw err;

                if (!user) {
                  res.json({ success: false, message: 'Authentication failed. User not found.' });
                } else if (user) {

                  // check if password matches
                  if (user.password != req.body.password) {
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                  } else {

                    // if user is found and password is right
                    // create a token
                    var token = jwt.sign(user, app.get('superSecret'), {
                      expiresInMinutes: 1440 // expires in 24 hours
                    });

                    // return the information including token as JSON
                    res.json({
                      success: true,
                      message: 'Auth Token.',
                      user: user,
                      token: token
                    });
                  }
                }
            });
        },
        verifyToken: function(req, res) {
            // check header or url parameters or post parameters for token
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            // decode token
            if (token) {

                // verifies secret and checks exp
                jwt.verify(token, app.get('superSecret'), function(err, decoded) {
                    if (err) {
                        return res.json({ success: false, message: 'Failed to authenticate token.' });
                    } else {
                        // if everything is good, save to request for use in other routes
                        req.decoded = decoded;
                        return res.json({ success: true, message: 'authenticated token.' });
                    }
                });

            } else {
                // if there is no token
                // return an error
                return res.status(403).send({
                    success: false,
                    message: 'No token provided.'
                });
            }

        },
        showUsers: function(req, res) {
            User.find({}, function(err, users) {
                res.json(users);
            });
        },
        createAdmin: function(req, res) {
            // create a sample user
            var usr = new User({
                name: 'Andre',
                password: 'admin',
                admin: true
            });

            // save the sample user
            usr.save(function(err) {
                if (err) throw err;

                console.log('User saved successfully');
                res.json({ success: true });
            });
        }
    };
    return authController;
};
