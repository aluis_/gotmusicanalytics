module.exports = function(app, jwt, User) {
    var createUser = {
        //REMOVE WHEN CREATE USER FORM IS WORKING
        createAbraham: function(req, res) {
            // create a sample user
            var usr = new User({
                name: 'Abraham',
                password: 'MellowYellow',
                admin: true,
                info: {
                    name: 'Abraham Mateo',
                    img: 'img/artistImgPlaceholder.png',
                    NBS_ARTIST_ID: '287163',
                    MESSAGE_FORM_ID: '1S5SP9g3fnNLFYsL3Abzk5eucDlIZFOzTslKqLQ-A5Yc'
                }
            });

            // save the sample user
            usr.save(function(err) {
                if (err) throw err;

                console.log('User saved successfully');
                res.json({ success: true });
            });
        }
    };

    return createUser;
};
